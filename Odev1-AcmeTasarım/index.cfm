 <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="main.css" type="text/css" media="all" rel="stylesheet"/>
    <title>ACME</title>
</head>
<body>
        
               
        <div class="container">
          <h2 style="text-align: center;">Hediye Gönderme Formu</h2>
          <form action="/action_page.php">
          <div class="row">
            <div class="col-25">
              <label for="name">Hediye Gönderen Adı</label>
            </div>
            <div class="col-75">
              <input type="text" id="name" name="name" readonly style="background-color: rgb(233, 229, 229);">
            </div>
          </div>
          
          <div class="row">
            <div class="col-25">
              <label for="gift">Hediye Seçiniz</label>
            </div>
            <div class="col-75">
              <select id="gift" name="gift">
                <option value="sec">Hediyenizi Seçiniz..</option>
                <option value="legoSeti">Lego Seti</option>
                <option value="kuruYemis">Kuru Yemiş Paketi</option>
                <option value="kahveMakinesi">Kahve Makinesi</option>
                <option value="piyangoBileti">Piyango Bileti</option>
                <option value="kaskol">Kaşkol</option>
              </select>
            </div>
          </div>

          <div class="row">
                <div class="col-25">
                  <label for="giftPerson">Gönderilecek Kişi Seçiniz</label>
                </div>
                <div class="col-75">
                  <select id="giftperson" name="giftPerson">
                        <option value="sec">Hediye Gönderilecek Kişiyi Seçiniz..</option>
                        <option value="anne">Anne</option>
                        <option value="baba">Baba</option>
                        <option value="cocuk">Çocuk</option>
                        <option value="es">Eş veya Sevgili</option>
                        <option value="kardes">Kardeş</option>
                        <option value="arkadas">Arkadaş</option>
                  </select>
                </div>
          </div>

          <div class="row">
              <div class="col-25">
                <label for="name">Hediye Gönderilecek Adı</label>
              </div>
              <div class="col-75">
                <input type="text" id="name" name="name" placeholder="Hediye Gönderilecek Adı..">
              </div>
            </div>

          <div class="row">
            <div class="col-25">
              <label for="subject">Gönderilecek Adres</label>
            </div>
            <div class="col-75">
              <textarea id="subject" name="subject" placeholder="gönderilecek adresi buraya yazınız.." style="height:100px"></textarea>
            </div>
          </div>

          <div class="row">
            <div class="col-25">
              <label for="subject">Mesaj</label>
            </div>
            <div class="col-75">
              <textarea id="subject" name="subject" placeholder="Mesajınızı buraya yazınız.." style="height:150px"></textarea>
            </div>
          </div>
          
          <div class="row">
            <input type="reset" name="clear" value="Temizle">
            <input type="submit" name="submit" value="Gönder">
          </div>
          </form>
        </div>
        
        </body>
</html>


<cfcomponent>
    <cffunction name="OgrenciGetir" returnformat="JSON" returntype="any">
        <cfargument name="ogrenci">
        <cfquery name="selectStudent" datasource="workcube_youngdev2">
            SELECT Ad,Soyad,Email
        <cfif structKeyExists(ogrenci,"DogumTarihi")>,DogumTarihi</cfif> 
        FROM Ogrenci 
        </cfquery>
        <cfreturn selectStudent>
    </cffunction>
</cfcomponent>
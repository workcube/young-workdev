<cfscript>
    catalogManagerCfc = createObject('component', 'cfc.catalogManagerApi');
</cfscript>
    <cfset listCatalogResponse = catalogManagerCfc.ListSelectCatalog()>
<cfhttp url="http://localhost:8500/rest/catalog/catalog/ListSelectCatalog" method="get" result="Liste">
</cfhttp>
    
    <!--- <cfdump var = "#listCatalogResponse.data#" abort>--->

    <cfoutput>
        <cfdiv id="right-side-catalog" class="row container">
                <div class="panel" style="padding:5px">
                    <div class="panel-heading">
                        <img id="catalog-image" src="../Odev3_Intranet/asest/img/panel.png" alt="...">
                    </div>
                    <div class="panel-body" >
                        <ul class="list-group">

                        <cfloop query="#listCatalogResponse.data#">
                            <li class="list-group-item-panel-top">
                                <div class="media-body">
                                    <a href="">
                                        <h6>#Title#</h6>
                                    </a>
                                    <small>#SubTitle#</small>
                                    <div>
                                        <i class="fa fa-play-circle"></i>
                                        <i class="fa fa-text-height"></i>
                                        <a href="" style="background-color: rgb(250, 222, 66); color: white;">İncele</a>
                                    </div>
                                </div>
                            </li>
                        </cfloop>
                            <li class="list-group-item-panel-footer">
                                <div class="media-body">
                                     <a href="">
                                         <small>Tüm katalogu incelemek için tıklayınız.</small>
                                </div>
                             </li>
                        </ul>
                    </div>
                </div>
        </cfdiv>

        <cfdiv id="right-side-sosial" class="row container">
                <div class="panel" style="padding:5px; width: 100%;">
                    <div class="panel-heading" style="border:solid 1px">
                        <img id="sosial-image" src="../Odev3_Intranet/asest/img/sosial.png"alt="...">
                    </div>
                    <div class="panel-body" >
                        <ul class="list-group">
                            <li class="list-group-item">
                                <div class="media-body">
                                    <div class="row">
                                        <div class="col col-6" id="div1" >
                                            <a href=""><i class="fa fa-facebook-square"></i> workcube</a><br>
                                            <a href=""><i class="fa fa-linkedin-square"></i> workcube</a><br>
                                            <a href=""><i class="fa fa-google-plus-square"></i> workcube-erp</a>
                                        </div>
                                        <div class="col col-6" id="div2">
                                            <a href=""><i class="fa fa-twitter-square"></i> erp-gundemi</a><br>
                                            <a href=""><i class="fa fa-instagram"></i> hayat-size-guzel</a><br>
                                            <a href=""><i class="fa fa-pinterest-square"></i> photomoto</a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
        </cfdiv>
    </cfoutput>

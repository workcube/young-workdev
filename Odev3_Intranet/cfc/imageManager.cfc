
<cfcomponent>
    <cfset dsn="workCubeDSN">
    <cffunction name = "SelectImage" returnType = "struct" access = "public" returnformat="JSON">
        <cftry>
            <cfquery name = "ListImages" datasource = "#dsn#">
                SELECT * FROM Images
            </cfquery>
            <cfset response = structNew()>
            <cfset response.Data = ListImages>
            <cfreturn ListImages>
            <cfcatch type = "any">
                <cfset response = structNew()>
                <cfset response.StatusCode = 400>
                <cfset response.Message = "İşlem Başarısız - #cfcatch.message#">
            </cfcatch>
            <cffinally>
                <cfreturn response>
            </cffinally>
        </cftry>
    </cffunction>
</cfcomponent>

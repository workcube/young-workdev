
<cfcomponent rest="true" restPath="catalog">
    <cfset dsn="workCubeDSN">
    <cffunction name ="ListSelectCatalog" returnType ="struct" access ="remote" returnformat="JSON" restPath="ListSelectCatalog" httpMethod="GET" produces="application/json">

        <cftry>
            <cfquery name = "ListCatalogs" datasource = "#dsn#">
                SELECT * FROM Catalogs
            </cfquery>
            <cfset response = structNew()>
            <cfset response.Data = ListCatalogs>
            <cfreturn ListCatalogs>
            <cfcatch type = "any">
                <cfset response = structNew()>
                <cfset response.StatusCode = 400>
                <cfset response.Message = "İşlem Başarısız - #cfcatch.message#">
            </cfcatch>
            <cffinally>
                <cfreturn response>
            </cffinally>
        </cftry>
    </cffunction>
</cfcomponent>

<!DOCTYPE html>
<html lang="en">
<head>
    <cfprocessingdirective pageencoding="UTF-8">
    
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <link rel="stylesheet" href="../Odev3_Intranet/asest/css/site.css" type="text/css" media="all">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    
    <title>INTRANET</title>
</head>
<body>
    <cfoutput>
        <cfdiv id="index" class="container-fluid">
            <cfinclude template="banner.cfm">

            <cfdiv class="row content" style="margin-top: 20px;">

                <cfdiv id="leftPage" class="col col-5 col-md-4 col-sm-4" style=" border-right: solid 1px;">
                    <cfinclude template="leftSide.cfm">
                </cfdiv>

                <cfdiv id="center" class="col col-2 col-md-4 col-sm-4" style="border-right: solid 1px;">
                    <cfinclude template="centerSide.cfm">
                </cfdiv>

                <cfdiv id="rightPage" class="col col-5 col-md-4 col-sm-4">
                    <cfinclude template="rightSide.cfm">
                </cfdiv>

            </cfdiv>
        </cfdiv>
    </cfoutput>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>
<cfprocessingdirective pageEncoding="utf-8">
<cfscript>
    restCfc = createObject('component','cfc.rest');
</cfscript>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ders5 - Cfc,Rest Api,CFQuery</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
<cfset AdSoyad = restCfc.returnString("Kaan","Salı")>
<!--- <cfset AcmeHediyeResponse = restCfc.AcmeHediyeGonder("workcube_youngdev","Ayşe","Ahmet","","","","Mehmet",5,3,0,0,0,4,"Kaan Salı","Katip Sokak Koşuyolu/Kadıköy","05333333333")>

 --->
 
 <cfset hediye=structNew()>
 <cfset hediye.dsn="workcube_youngdev">
 <cfset hediye.AnneAdi="Ayşe">
 <cfset hediye.BabaAdi="Ahmet">
 <cfset hediye.KardesAdi="">
 <cfset hediye.EsAdi="">
 <cfset hediye.CocukAdi="">
 <cfset hediye.ArkadasAdi="Mehmet">
 <cfset hediye.AnneHediye=5>
 <cfset hediye.BabaHediye=3>
 <cfset hediye.KardesHediye=0>
 <cfset hediye.EsHediye=0>
 <cfset hediye.CocukHediye=0>
 <cfset hediye.ArkadasHediye=4>
 <cfset hediye.GonderenAdi="Kaan Salı">
 <cfset hediye.Adres="Katip Sokak Koşuyolu/Kadıköy">
 <cfset hediye.TelNo="05333333333">
<!--- CFHttp ile veri gönderme işlemi gerçekleştirdik. --->
<!--- <cfhttp url="localhost:8500/rest/acme/acme/AcmeHediyeGonder" method="post" result="result">
    <cfhttpparam name="Content-Type" value="application/json" type="header">
    <cfhttpparam name="data" value="#serializeJSON(hediye)#" type="body">
</cfhttp> --->
<cfhttp url="localhost:8500/rest/acme/acme/HediyeListesi" method="get" result="Liste">
    <cfhttpparam name="Content-Type" value="application/json" type="header">
</cfhttp>
<!---    <cfdump var="#replace(replace(replace(serializeJson(deserializeJson(Liste.filecontent).columns),"[",""),"]",""),"""","","all")#" abort>
 ---><body>
    <div id="RestRequest">
        <table border="1">
            <thead>
                <th>Anne Adı</th>
                <th>Anne Hediye</th>
                <th>Gönderen Adı</th>
                <th>Adres</th>
            </thead>
            <tbody id="tableBody">
                <cfscript>
                    listequery = QueryNew(replace(replace(replace(serializeJson(deserializeJson(Liste.filecontent).columns),"[",""),"]",""),"""","","all"),"Varchar,Integer,Varchar,Varchar",deserializeJson(Liste.filecontent).Data)
                </cfscript>
                <cfloop query="listequery"<!---  --->>
                    <tr>
                        <td>
                            <cfoutput>#AnneAdi#</cfoutput>
                        </td>
                        <td>
                            <cfoutput>#AnneHediye#</cfoutput>
                        </td>
                        <td>
                            <cfoutput>#GonderenAdi#</cfoutput>
                        </td>
                        <td>
                            <cfoutput>#Adres#</cfoutput>
                        </td>
                    </tr>
                </cfloop>
                <cfloop array="#deserializeJson(Liste.filecontent).data#" index="i">
                <tr>
                    <td>
                        <cfoutput>#i[1]#</cfoutput>
                    </td>
                    <td>
                        <cfoutput>#i[2]#</cfoutput>
                    </td>
                    <td>
                        <cfoutput>#i[3]#</cfoutput>
                    </td>
                    <td>
                        <cfoutput>#i[4]#</cfoutput>
                    </td>
                </tr>
            </cfloop>
            </tbody>
        </table>
        <div>
            <input type="text" name="AnneAdi" id="AnneAdi">
            <select id="AnneHediye">
                <option value=1>Lego Seti</option>
                <option value=2>Kahve Makinesi</option>
                <option value=5>Kaşkol</option>
            </select>
            <input type="text" name="GonderenAdi" id="GonderenAdi">
            <input type="textarea" name="Adres" id="Adres">
            <button onclick="insertHediye()">Hediye Gönder</button>
        </div> 
        <p id="RestMessage">İşlem Bekleniyor.</p>
    </div>
    <cfoutput>
<!---        <cfdump var=#result.filecontent# abort>
       <cfoutput>
           <h1>#AcmeHediyeResponse.Message#</h1>
           <h2>#AcmeHediyeResponse.StatusCode#</h2>
       </cfoutput> --->
    </cfoutput>
</body>
</html>

<script>
    function insertHediye(){
        var dsn = "workcube_youngdev";
        var AnneAdi = $("#AnneAdi")[0].value;
        var AnneHediye = $("#AnneHediye")[0].value; //document.getElementById("AnneHediye").value;
        var GonderenAdi = document.getElementById("GonderenAdi").value;
        var Adres = document.getElementById("Adres").value;
        var BabaAdi="Ahmet";
        var KardesAdi="";
        var EsAdi="";
        var CocukAdi="";
        var ArkadasAdi="Mehmet";
        var BabaHediye=3;
        var KardesHediye=0;
        var EsHediye=0;
        var CocukHediye=0;
        var ArkadasHediye=4;
        var TelNo="05333333333";
        
        var data ={
            "AnneAdi" : AnneAdi,
            "AnneHediye" : AnneHediye,
            "GonderenAdi" : GonderenAdi,
            "Adres":Adres,
            "dsn" : dsn,
            "BabaAdi" : BabaAdi,
            "KardesAdi" : KardesAdi,
            "EsAdi" : EsAdi,
            "CocukAdi" : CocukAdi,
            "ArkadasAdi" : ArkadasAdi,
            "BabaHediye" : BabaHediye,
            "KardesHediye" : KardesHediye,
            "EsHediye" : EsHediye,
            "CocukHediye" : CocukHediye,
            "ArkadasHediye" : ArkadasHediye,
            "TelNo" : TelNo
        };
        $.ajax({
            headers: { 
                'Accept': 'application/json',
                'Content-Type': 'application/json;charset=utf-8'
            },
            url:"http://localhost:8500/rest/acme/acme/AcmeHediyeGonder",
            type:"POST",
            data:JSON.stringify(data),
            success: function(resp) {
                document.getElementById("RestMessage").innerHTML = "<p>"+ resp.MESSAGE +"</p>";
                if(resp.STATUSCODE == 200){
                    $("#tableBody").append("<tr><td>"+AnneAdi+"</td><td>"+AnneHediye+"</td><td>"+GonderenAdi+"</td><td>"+"Adres"+"</td></tr>");
                }
            }
        });

        
    }
</script>
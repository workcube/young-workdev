<cfcomponent rest="true" restPath="acme">
    <cfset dsn="workcube_youngdev">
    <cffunction name="returnString" access="remote" returnformat="JSON" returntype="struct" restPath="returnString" httpMethod="GET">
        <cfargument name="ad" type="string" displayname="ad" default="İsimsiz">
        <cfargument name="soyad" type="string" displayname="soyad" default="Soyisimsiz">

        <cfset response = structNew()>
        <cfset response.AdSoyad = ad & " " & soyad>
        <cfreturn response>
    </cffunction>
    <!--- public string returnString(ad,soyad){
        return ad + " " + soyad;
    } --->
    <cffunction name="AcmeHediyeGonder" access="remote" returnformat="JSON" returntype="struct" restPath="AcmeHediyeGonder" httpMethod="POST" produces="application/json">
        <cfargument name="data" type="struct">
        <cftry>      
        <cfquery datasource="#data.dsn#">
            INSERT INTO Acme_Ders5 (
                    AnneAdi,
                    BabaAdi,
                    KardesAdi,
                    EsAdi,
                    CocukAdi,
                    ArkadasAdi,
                    AnneHediye,
                    BabaHediye,
                    KardesHediye,
                    EsHediye,
                    CocukHediye,
                    ArkadasHediye,
                    TelNo,
                    GonderenAdi,
                    Adres
                ) VALUES
                (
                    <cfqueryparam cfsqltype="cf_sql_nvarchar" value="#data.AnneAdi#">,
                    <cfqueryparam cfsqltype="cf_sql_nvarchar" value="#data.BabaAdi#">,
                    <cfqueryparam cfsqltype="cf_sql_nvarchar" value="#data.KardesAdi#">,
                    <cfqueryparam cfsqltype="cf_sql_nvarchar" value="#data.EsAdi#">,
                    <cfqueryparam cfsqltype="cf_sql_nvarchar" value="#data.CocukAdi#">,
                    <cfqueryparam cfsqltype="cf_sql_nvarchar" value="#data.ArkadasAdi#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#data.AnneHediye#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#data.BabaHediye#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#data.KardesHediye#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#data.EsHediye#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#data.CocukHediye#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#data.ArkadasHediye#">,
                    <cfqueryparam cfsqltype="cf_sql_nvarchar" value="#data.TelNo#">,
                    <cfqueryparam cfsqltype="cf_sql_nvarchar" value="#data.GonderenAdi#">,
                    <cfqueryparam cfsqltype="cf_sql_nvarchar" value="#data.Adres#">
                )
        </cfquery>

            <cfset response = structNew()>
            <cfset response.StatusCode = 200>
            <cfset response.Message = "İşlem Başarıyla Tamamlandı">
        <cfcatch type="any">
            <cfset response = structNew()>
            <cfset response.StatusCode = 400>
            <cfset response.Message = "İşlem Başarısız - #cfcatch.message#">
        </cfcatch>
        <cffinally>
            <cfreturn response>
        </cffinally>
        </cftry>
    </cffunction>

    <cffunction name="HediyeListesi" access="remote" returnformat="JSON" returntype="query" restPath="HediyeListesi" produces="application/json" httpMethod="GET">
        <cfquery name="HediyeListesi" datasource="#dsn#">
            SELECT AnneAdi,AnneHediye,GonderenAdi,Adres FROM Acme_Ders5
        </cfquery>
        <cfreturn HediyeListesi>
    </cffunction>

</cfcomponent>


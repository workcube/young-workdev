
<cfcomponent>
    <cfset dsn = "workCubeAcme">
    <cffunction name = "GetBranchList" returnType = "struct" access = "public" returnFormat="JSON">
        <cftry>
            <cfquery name = "GetBranchList" datasource = "#dsn#">
               select b.Id, b.Name from Branch b 
            </cfquery>
            <cfset response = structNew()>
            <cfset response.Data = GetBranchList>
            <cfreturn GetBranchList>
            <cfcatch type = "any">
                <cfset response = structNew()>
                <cfset response.StatusCode = 400>
                <cfset response.Message = "İşlem Başarısız - #cfcatch.message#">
            </cfcatch>
            <cffinally>
                <cfreturn response>
            </cffinally>  
        </cftry>
    </cffunction>

    <cffunction name = "GetBranchCategory" returnType = "struct" access = "public" returnFormat="JSON">
        <cfargument name="branchId">
        <cftry>
            <cfquery name = "GetBranchCategory" datasource = "#dsn#">
                declare @branchId integer
                Set @branchId = #branchId#

                SELECT 
                b.Name AS [Sube_Adi],c.Name as [Kategori_Adi],
                COUNT(oi.Quantity) as Adet, SUM(o.OrderTotal) as [Toplam_Tutar] 
                from [Order] o 
                join OrderItem oi on o.Id = oi.OrderId
                join Employee e on o.EmployeeId = e.Id
                join Branch b on e.BranchId = b.Id
                join Product p on oi.ProductId = p.Id
                join Category c on p.CategoryId = c.Id
                where b.Id = @branchId
                group by c.Name, b.Name
            </cfquery>
            <cfset response = structNew()>
            <cfset response.Data = GetBranchCategory>
            <cfreturn GetBranchCategory>
            <cfcatch type = "any">
                <cfset response = structNew()>
                <cfset response.StatusCode = 400>
                <cfset response.Message = "İşlem Başarısız - #cfcatch.message#">
            </cfcatch>
            <cffinally>
                <cfreturn response>
            </cffinally>  
        </cftry>
    </cffunction>

    <cffunction name = "GetListBranchCategory" returnType = "struct" access = "public" returnFormat="JSON">
        
        <cftry>
            <cfquery name = "GetListBranchCategory" datasource = "#dsn#">
                select b.Name as [Sube_Adi], c.Name as [Kategori_Adi],COUNT(oi.Quantity) as Adet, SUM(o.OrderTotal) as [Toplam_Tutar] 
                    from [Order] o 
                    join OrderItem oi on o.Id = oi.OrderId
                    join Employee e on o.EmployeeId = e.Id
                    join Branch b on e.BranchId = b.Id
                    join Product p on oi.ProductId = p.Id
                    join Category c on p.CategoryId = c.Id
                    group by c.Name, b.Name
                    order by b.Name
            </cfquery>
            <cfset response = structNew()>
            <cfset response.Data = GetListBranchCategory>
            <cfreturn GetListBranchCategory>
            <cfcatch type = "any">
                <cfset response = structNew()>
                <cfset response.StatusCode = 400>
                <cfset response.Message = "İşlem Başarısız - #cfcatch.message#">
            </cfcatch>
            <cffinally>
                <cfreturn response>
            </cffinally>  
        </cftry>
    </cffunction>
</cfcomponent>


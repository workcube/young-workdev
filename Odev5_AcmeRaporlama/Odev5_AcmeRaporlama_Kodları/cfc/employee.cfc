<cfcomponent>
    <cfset dsn = "workCubeAcme">
     <cffunction name = "GetListEmployee" returnType = "struct" access = "public" returnFormat="JSON">
        <cftry>
            <cfquery name = "GetListEmployee" datasource = "#dsn#">
                select e.Name as Calisan_Ad, e.Surname as Calisan_Soyad, b.Name as Sube_Ad from Employee e
                join Branch b on b.Id = e.BranchId
                order by b.Id
            </cfquery>
            <cfset response = structNew()>
            <cfset response.Data = GetListEmployee>
            <cfreturn GetListEmployee>
            <cfcatch type = "any">
                <cfset response = structNew()>
                <cfset response.StatusCode = 400>
                <cfset response.Message = "İşlem Başarısız - #cfcatch.message#">
            </cfcatch>
            <cffinally>
                <cfreturn response>
            </cffinally>  
        </cftry>
    </cffunction>
    <cffunction name = "GetEmployeeSoldCategory" returnType = "struct" access = "public" returnFormat="JSON">
        <cfargument name="employeeId">
        <cftry>
            <cfquery name = "GetEmployeeSoldCategory" datasource = "#dsn#">
                
                select e.Name as Calısan_Ad,e.Surname as Calisan_Soyad, c.Name as [Kategori_Adi], COUNT(o.EmployeeId) as [Satis_Adeti] from Employee e
                join [Order] o on e.Id = o.EmployeeId
                join OrderItem oi on o.Id = oi.OrderId
                join Product p on p.Id = oi.ProductId
                join Category c on c.Id = p.CategoryId
                where e.Id = #employeeId#
                group by c.Name,e.Name, e.Surname
                order by [Satis_Adeti]
            </cfquery>
            <cfset response = structNew()>
            <cfset response.Data = GetEmployeeSoldCategory>
            <cfreturn GetEmployeeSoldCategory>
            <cfcatch type = "any">
                <cfset response = structNew()>
                <cfset response.StatusCode = 400>
                <cfset response.Message = "İşlem Başarısız - #cfcatch.message#">
            </cfcatch>
            <cffinally>
                <cfreturn response>
            </cffinally>  
        </cftry>
    </cffunction>
    <cffunction name = "GetCategorySoldEmployee" returnType = "struct" access = "public" returnFormat="JSON">
        <cfargument name="categoryId">
        <cftry>
            <cfquery name = "GetCategorySoldEmployee" datasource = "#dsn#">
                
                select e.Name as Calısan_Ad,e.Surname as Calisan_Soyad, c.Name as [Kategori_Adi], COUNT(o.EmployeeId) as [Satis_Adedi] from Employee e
                join [Order] o on e.Id = o.EmployeeId
                join OrderItem oi on o.Id = oi.OrderId
                join Product p on p.Id = oi.ProductId
                join Category c on c.Id = p.CategoryId
                where c.Id = #categoryId#
                group by c.Name,e.Name, e.Surname 
                order by Satis_Adedi
            </cfquery>
            <cfset response = structNew()>
            <cfset response.Data = GetCategorySoldEmployee>
            <cfreturn GetCategorySoldEmployee>
            <cfcatch type = "any">
                <cfset response = structNew()>
                <cfset response.StatusCode = 400>
                <cfset response.Message = "İşlem Başarısız - #cfcatch.message#">
            </cfcatch>
            <cffinally>
                <cfreturn response>
            </cffinally>  
        </cftry>
    </cffunction>
    <cffunction name = "GetCategoryDateEmployee" returnType = "struct" access = "remote" returnFormat="JSON">
        <cfargument name="date">
        <cfargument name="categoryId">
        <cftry>
            <cfquery name = "GetCategoryDateEmployee" datasource = "#dsn#">
                select TBL.Ay, TBL.Name,TBL.Surname, TBL.Kategori_Adi, sum(TBL.toplam)as Toplam_Adet from
                    (SELECT 
                        CASE
                            WHEN MONTH(o.ItemSoldDate) = '1'  THEN 'OCAK'
                            WHEN MONTH(o.ItemSoldDate) = '2'  THEN 'ŞUBAT'
                            WHEN MONTH(o.ItemSoldDate) = '3'  THEN 'MART'
                            WHEN MONTH(o.ItemSoldDate) = '4'  THEN 'NİSAN'
                            WHEN MONTH(o.ItemSoldDate) = '5'  THEN 'MAYIS'
                            WHEN MONTH(o.ItemSoldDate) = '6'  THEN 'HAZİRAN'
                            WHEN MONTH(o.ItemSoldDate) = '7'  THEN 'TEMMUZ'
                            WHEN MONTH(o.ItemSoldDate) = '8'  THEN 'AĞUSTOS'
                            WHEN MONTH(o.ItemSoldDate) = '9'  THEN 'EYLÜL'
                            WHEN MONTH(o.ItemSoldDate) = '10'  THEN 'EKİM'
                            WHEN MONTH(o.ItemSoldDate) = '11'  THEN 'KASIM'
                            WHEN MONTH(o.ItemSoldDate) = '12'  THEN 'ARALIK'
                        END AS Ay,e.Id,e.Name, e.Surname, c.Name as Kategori_Adi,count(o.Id) as toplam from [Order] o
                        join OrderItem oi on oi.OrderId = o.Id
                        join Product p on p.Id = oi.ProductId
                        join Category c on c.Id = p.CategoryId
                        join Employee e on e.Id = o.EmployeeId
                        where MONTH(o.ItemSoldDate) = #date# and c.Id = #categoryId#
                        group by e.Id, e.Name,e.Surname,c.Name,o.ItemSoldDate)TBL
                    group by TBL.Ay, TBL.Name, TBL.Surname, TBL.Kategori_Adi
                    order by Toplam_Adet
            </cfquery>
            <cfset response = structNew()>
            <cfset response.Data = GetCategoryDateEmployee>
            <cfreturn GetCategoryDateEmployee>
            <cfcatch type = "any">
                <cfset response = structNew()>
                <cfset response.StatusCode = 400>
                <cfset response.Message = "İşlem Başarısız - #cfcatch.message#">
            </cfcatch>
            <cffinally>
                <cfreturn response>
            </cffinally>  
        </cftry>
    </cffunction>
</cfcomponent>
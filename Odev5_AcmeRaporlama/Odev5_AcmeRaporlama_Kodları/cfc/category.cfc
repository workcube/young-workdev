
<cfcomponent>
    <cfset dsn = "workCubeAcme">
     <cffunction name = "GetListCategory" returnType = "struct" access = "public" returnFormat="JSON">
        <cftry>
            <cfquery name = "GetListCategory" datasource = "#dsn#">
                select Id, Name from Category c
            </cfquery>
            <cfset response = structNew()>
            <cfset response.Data = GetListCategory>
            <cfreturn GetListCategory>
            <cfcatch type = "any">
                <cfset response = structNew()>
                <cfset response.StatusCode = 400>
                <cfset response.Message = "İşlem Başarısız - #cfcatch.message#">
            </cfcatch>
            <cffinally>
                <cfreturn response>
            </cffinally>  
        </cftry>
    </cffunction>
    <cffunction name = "GetSoldCategory" returnType = "struct" access = "public" returnFormat="JSON">
        <cfargument name="queryDate">
        <cftry>
            <cfquery name = "GetSoldCategory" datasource = "#dsn#">
                declare @queryDate integer
                Set @queryDate = #queryDate#
                SELECT c.[Name] AS [Kategori_Adi],
                CASE
                    WHEN MONTH(o.ItemSoldDate) = '1'  THEN 'OCAK'
                    WHEN MONTH(o.ItemSoldDate) = '2' THEN 'ŞUBAT'
                    WHEN MONTH(o.ItemSoldDate) = '3'  THEN 'MART'
                    WHEN MONTH(o.ItemSoldDate) = '4'  THEN 'NİSAN'
                    WHEN MONTH(o.ItemSoldDate) = '5'  THEN 'MAYIS'
                    WHEN MONTH(o.ItemSoldDate) = '6'  THEN 'HAZİRAN'
                    WHEN MONTH(o.ItemSoldDate) = '7'  THEN 'TEMMUZ'
                    WHEN MONTH(o.ItemSoldDate) = '8'  THEN 'AĞUSTOS'
                    WHEN MONTH(o.ItemSoldDate) = '9'  THEN 'EYLÜL'
                    WHEN MONTH(o.ItemSoldDate) = '10'  THEN 'EKİM'
                    WHEN MONTH(o.ItemSoldDate) = '11'  THEN 'KASIM'
                    WHEN MONTH(o.ItemSoldDate) = '12'  THEN 'ARALIK'
                END AS Ay,
                SUM(o.OrderTotal) AS [Toplam_Tutar],sum(p.CategoryId) as Adet
                FROM [Order]  o 
                INNER JOIN OrderItem oi ON o.Id = oi.Id
                INNER JOIN Product p ON p.Id = oi.ProductId
                INNER JOIN Category c ON c.Id = p.CategoryId
                where o.OrderTotal != 0 and MONTH(o.ItemSoldDate) = @queryDate
                GROUP BY MONTH(o.ItemSoldDate),c.[Name]
                ORDER BY MONTH(o.ItemSoldDate),c.[Name]
            </cfquery>
            <cfset response = structNew()>
            <cfset response.Data = GetSoldCategory>
            <cfreturn GetSoldCategory>
            <cfcatch type = "any">
                <cfset response = structNew()>
                <cfset response.StatusCode = 400>
                <cfset response.Message = "İşlem Başarısız - #cfcatch.message#">
            </cfcatch>
            <cffinally>
                <cfreturn response>
            </cffinally>  
        </cftry>
    </cffunction>
</cfcomponent>


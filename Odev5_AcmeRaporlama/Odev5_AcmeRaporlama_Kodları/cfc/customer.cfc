
<cfcomponent>
    <cfset dsn = "workCubeAcme">
    <cffunction name = "GetAvgTotal" returnType = "struct" access = "public" returnFormat="JSON">
        <cftry>
            <cfquery name = "GetAvgTotal" datasource = "#dsn#">
                select TBL1.Grup_Adi as Grup_Adi, count(TBL1.Adet) as Adet from
                    (SELECT 
                    CASE
                        WHEN TBL.Ortalama_Tutar <= 1000  THEN 'Grup1_1000-'
                        WHEN TBL.Ortalama_Tutar > 1000 and TBL.Ortalama_Tutar <= 2000 THEN 'Grup2_1000-2000'
                        WHEN TBL.Ortalama_Tutar > 2000 and TBL.Ortalama_Tutar <= 3000 THEN 'Grup3_2000-3000'
                        WHEN TBL.Ortalama_Tutar > 3000 and TBL.Ortalama_Tutar <= 4000 THEN 'Grup4_3000-4000'
                        WHEN TBL.Ortalama_Tutar > 4000  THEN 'Grup5_4000+'
                        
                    END AS [Grup_Adi],COUNT(TBL.Musteri_Adi) as Adet
                    from 
                        (select  c.Name as [Musteri_Adi],c.Surname, cast(Avg(o.OrderTotal)as decimal(15,2)) as [Ortalama_Tutar] from [Order] o
                        join OrderItem oi on o.Id = oi.OrderId
                        join Customer c on o.CustomerId = c. Id
                        group by c.Name,c.Surname, o.OrderTotal) TBL
                    group by TBL.Ortalama_Tutar)TBL1
                    group by TBL1.Grup_Adi
                    order by TBL1.Grup_Adi
            </cfquery>
            <cfset response = structNew()>
            <cfset response.Data = GetAvgTotal>
            <cfreturn GetAvgTotal>
            <cfcatch type = "any">
                <cfset response = structNew()>
                <cfset response.StatusCode = 400>
                <cfset response.Message = "İşlem Başarısız - #cfcatch.message#">
            </cfcatch>
            <cffinally>
                <cfreturn response>
            </cffinally>  
        </cftry>
    </cffunction>
</cfcomponent>



<cfprocessingdirective pageencoding="utf-8" />

<cfscript>
    customerCfc = createObject('component','cfc.customer');
</cfscript>

<!DOCTYPE html>
<html lang="en">
  <head>

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="../Odev3_Intranet/asest/css/site.css" type="text/css" media="all">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <title>RAPORLAR</title>
  </head>
   <cfparam  name="no" default="1">
   <cfset customerCfcAvgResponse = customerCfc.GetAvgTotal()> 
   <cfset  data = customerCfcAvgResponse> 
  <!---<cfdump var = "#customerCfcAvgResponse#" abort>
--->
  <body>
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="../Odev5_AcmeRaporlama/index.cfm">Acme</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item active">
              <a class="nav-link" href="../Odev5_AcmeRaporlama/index.cfm">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../Odev5_AcmeRaporlama/customer.cfm">Customer</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../Odev5_AcmeRaporlama/category.cfm">Category</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../Odev5_AcmeRaporlama/branch.cfm">Branch</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../Odev5_AcmeRaporlama/employee.cfm">Employee</a>
            </li>
          </ul>
        </div>
      </nav>
    <div class="container">

      <div class="row" style="margin-top: 15px;">
        <div class="col col-md-12">
            <h3 style="color: rgb(136, 25, 25);">Müşterilerin Toplam Alışveriş Ortalamalarına Göre Gruplama Raporu</h3>
        </div>
      
        <div class="row">
            <div class="col col-md-12" name="tableRow">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Grup Adı</th>
                            <th scope="col">Toplam Adet</th>
                        </tr>
                    </thead>
                    <tbody>
                        <cfoutput>
                            <cfloop query=#data.data#>
                                <tr>
                                    <td scope="row">#no#</td>
                                    <td>#Grup_Adi#</td>
                                    <td>#Adet#</td>
                                </tr>
                                <cfset no = no+1>
                            </cfloop>
                        </cfoutput>
                    </tbody>
                </table>
            </div>
            <div class="row" name="chartBarRow" >
                <div class="col col-md-12">
                  <div class="card">
                    <div class="card-header">
                        Müşteri Grubu-Ortalama Satış Grafiği
                    </div>
                    <div class="card-body">
                        <cfoutput>
                            <cfchart
                                format="png"
                                scalefrom="0"
                                scaleto="3000"
                                chartheight=300
                                chartwidth=600>
                                <cfchartseries
                                    type="bar"
                                    seriescolor="blue">
                                    <cfloop query=#data.data#>
                                   
                                      <cfchartdata item="#Grup_Adi#" value="#Adet#">
                                      
                                    </cfloop>
                                </cfchartseries>
                            </cfchart>
                        </cfoutput>
                    </div>
                  </div>
                </div>
            </div>
            <div class="row" name="chartPieRow" >
                <div class="col col-md-12">
                  <div class="card">
                    <div class="card-header">
                        Müşteri Grubu-Ortalama Satış Grafiği
                    </div>
                    <div class="card-body">
                        <cfoutput>
                            <cfchart
                                format="png"
                                scalefrom="100000"
                                scaleto="10000000"
                                chartheight=300
                                chartwidth=600>
                                <cfchartseries
                                    type="pie">
                                    <cfloop query=#data.data#>
                                      <cfchartdata item="#Grup_Adi#" value="#Adet#">
                                    </cfloop>
                                </cfchartseries>
                            </cfchart>
                        </cfoutput>
                    </div>
                  </div>
                </div>
            </div>
        </div>
      </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>

<cfprocessingdirective pageencoding="utf-8" />

<cfscript>
    branchCfc = createObject('component','cfc.branch');
</cfscript>

<!DOCTYPE html>
<html lang="en">
  <head>

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <title>RAPORLAR</title>
  </head>
   <cfset branchListCfcResponse = branchCfc.GetListBranchCategory()> 
   <cfset  data = branchListCfcResponse> 
  <!---<cfdump var = "#(form.FormInputBeginDate)#" abort>
--->
  <body>
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <cfinclude template="banner.cfm">
      </nav>
    <div class="container">
      <div class="row">
        <div class="col col-md-12"> 
          <h3 style="color: rgb(136, 25, 25);">Şubelere Göre Kategorilerin Satış Tablosu!</h3>
          <p>Diğer Özel Raporları Kullanmak için Menüyü Kullanın</p>
        </div>
              <div class="col col-md-12" name="tableRow">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">Şube Adı</th>
                            <th scope="col">Kategori Adı</th>
                            <th scope="col">Toplam Adet</th>
                            <th scope="col">Toplam Tutar</th>
                        </tr>
                    </thead>
                    <tbody>
                        <cfoutput>
                            <cfloop query=#data.data#>
                                <tr>
                                    <td scope="row" style="color: red;"><b>#Sube_Adi#</b></td>
                                    <td>#Kategori_Adi#</td>
                                    <td>#Adet#</td>
                                    <td>#Toplam_Tutar#</td>
                                </tr>
                            </cfloop>
                        </cfoutput>
                    </tbody>
                </table>
              </div>
      </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
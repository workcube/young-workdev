
<cfprocessingdirective pageencoding="utf-8" />

<cfscript>
    employeeCfc = createObject('component','cfc.employee');
    categoryCfc = createObject('component','cfc.category');
</cfscript>

<!DOCTYPE html>
<html lang="en">
  <head>

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">   
    <link rel="stylesheet" href="../Odev5_AcmeRaporlama/asest/css/site.css" type="text/css" media="all">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <title>RAPORLAR</title>
  </head>
  <cfset categoryCfcListResponse = categoryCfc.GetListCategory()>
  <cfset  data = categoryCfcListResponse>
   <!--- <cfset employeeCfcResponse = employeeCfc.GetListEmployee()> 
   <cfset  data = employeeCfcResponse> --->
   
  <!--- <cfdump var = "#dataGetCategory.data#" abort> --->

  <body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <cfinclude template="banner.cfm">
      </nav>
    <div class="container">
            <div class="tab-content">
                <div class="card-header">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                          <a class="nav-link active" id="category-tab" data-toggle="tab" href="#categoryReport" role="tab" aria-controls="category" aria-selected="true">Kategori Rapor</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" id="date-tab" data-toggle="tab" href="#dateReport" role="tab" aria-controls="date" aria-selected="false">Tarih Rapor</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" id="product-tab" data-toggle="tab" href="#productReport" role="tab" aria-controls="product" aria-selected="false">Ürün Rapor</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="customer-tab" data-toggle="tab" href="#customerReport" role="tab" aria-controls="customer" aria-selected="false">Müşteri Rapor</a>
                          </li>
                        <li class="nav-item">
                            <a class="nav-link" id="employee-tab" data-toggle="tab" href="#employeeReport" role="tab" aria-controls="employee" aria-selected="false">Çalışan Rapor</a>
                        </li>
                      </ul>
                </div>

                <div role="tabpanel" class="tab-pane active" id="categoryReport">
                    <div class="card col col-md-12">
                        
                        <div class="card-body">
                            <h5 class="card-title">Kategoriye Göre Çalışan-Satış Raporu</h5>
                            <form name="CategoySatis" method="POST" action="">
                                <div class="form-row align-items-center">
                                    <div class="col-sm-3 my-1 ">
                                        <input type="hidden" name="is_submit" id="is_submit" value="1">
                                        <label>Kategori Seçin</label>
                                        <select class="form-control" name="FormInputCategoryName" id="FormInputCategoryName">
                                            <option value='0'>Kategori Seçin</option>
                                            <cfoutput>
                                                <cfloop query=#data.data#>
                                                    <option value='#Id#'>#Name#</option>
                                                </cfloop>
                                            </cfoutput>
                                        </select>
                                    </div>
                                    <div class="col-sm-3 my-1">
                                        <input class="btn btn-primary" type="submit" value="Ara" style="margin-top: 32px;">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col col-md-12" name="tableRow">
                            <table class="table table-striped table-bordered">
                                <cfparam  name="no" default="1">
                                <thead>
                                    <tr>
                                        <th scope="col">Sıra No</th>
                                        <th scope="col">Çalışan Adı</th>
                                        <th scope="col">Çalışan Soyadı</th>
                                        <th scope="col">Kategori Adı</th>
                                        <th scope="col">Satış Adedi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <cfparam  name="FormInputCategoryName" default="0">
                                    <cfset GetCategoryResponse = employeeCfc.GetCategorySoldEmployee(#FormInputCategoryName#)> 
                                    <cfset  dataGetCategory = GetCategoryResponse> 
                                    <cfoutput>
                                        <cfloop query=#dataGetCategory.data#>
                                            <tr>
                                                <td scope="row">#no#</td>
                                                <td>#Calisan_Ad#</td>
                                                <td>#Calisan_Soyad#</td>
                                                <td>#Kategori_Adi#</td>
                                                <td>#Satis_Adedi#</td>
                                            </tr>
                                            <cfset no = no+1>
                                        </cfloop>
                                    </cfoutput>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                </div>
            
                <div role="tabpanel" class="tab-pane" id="dateReport">
                    <div class="card col col-md-12" id="dateReport1">
                        <div class="card-body" >
                            <h5 class="card-title">Tarihe Göre Çalışan-Kategori Satış Raporu</h5>
                            <form name="CategoySatis" method="POST" action="">
                                <div class="form-row align-items-center">
                                    <div class="col-sm-3 my-1 ">
                                        <input type="hidden" name="is_submit" id="is_submit" value="1">
                                        <label>Ay Seçin</label>
                                        <select class="form-control" name="FormInputDate" id="FormInputDate">
                                            <option value='0'>Ay Seçin</option>
                                            <option value='1'>Ocak</option>
                                            <option value='2'>Şubat</option>
                                            <option value='3'>Mart</option>
                                            <option value='4'>Nisan</option>
                                            <option value='5'>Mayıs</option>
                                            <option value='6'>Haziran</option>
                                            <option value='7'>Temmuz</option>
                                            <option value='8'>Ağustos</option>
                                            <option value='9'>Eylül</option>
                                            <option value='10'>Ekim</option>
                                            <option value='11'>Kasım</option>
                                            <option value='12'>Aralık</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3 my-1 ">
                                        <label>Kategori Seçin</label>
                                        <select class="form-control" name="FormInputCategoryName1" id="FormInputCategoryName1">
                                            <option value='0'>Kategori Seçin</option>
                                            <cfoutput>
                                                <cfloop query=#data.data#>
                                                    <option value='#Id#'>#Name#</option>
                                                </cfloop>
                                            </cfoutput>
                                        </select>
                                    </div>
                                    <div class="col-sm-3 my-1">
                                        <input class="btn btn-primary" type="button" onclick="getDateCategories()" value="Ara" style="margin-top: 32px;">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col col-md-12" name="tableRow">
                            <table class="table table-striped table-bordered">
                                <cfparam  name="form.FormInputDate" default= "0">
                                <cfparam  name="form.FormInputCategoryName1" default= "0">
                                <cfset employeeCfcCategoryDateResponse = employeeCfc.GetCategoryDateEmployee(#form.FormInputDate#,#form.FormInputCategoryName1#)>
                               
                                <thead>
                                    <tr>
                                        <th scope="col">Sıra No</th>
                                        <th scope="col">Ay</th>
                                        <th scope="col">Çalışan Adı</th>
                                        <th scope="col">Çalışan Soyadı</th>
                                        <th scope="col">Kategori Adı</th>
                                        <th scope="col">Toplam Adet</th>
                                    </tr>
                                </thead>
                                <tbody id="table_categoryDate_body">
                                    <cfoutput>
                                        <cfloop query=#employeeCfcCategoryDateResponse.data#>
                                            <tr>
                                                <td scope="row">#no#</td>
                                                <td>#Ay#</td>
                                                <td>#Name#</td>
                                                <td>#Surname#</td>
                                                <td>#Kategori_Adi#</td>
                                                <td>#Toplam_Adet#</td>
                                            </tr>
                                        </cfloop>
                                    </cfoutput>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div role="tabpanel" class="tab-pane" id="productReport">
                    <div class="card col col-md-12" id="productReport1">
                        <div class="card-body">
                            <h5 class="card-title">Ürün Bazlı Raporlar</h5>
                        </div>
                    </div>
                </div>

                <div role="tabpanel" class="tab-pane" id="customerReport" >
                    <div class="card col col-md-12" id="customerReport1">
                        <div class="card-body">
                            <h5 class="card-title">Müşteri Bazlı Raporlar</h5>
                        </div>
                    </div>
                </div>

                <div role="tabpanel" class="tab-pane" id="employeeReport" >
                    <div class="card col col-md-12" id="employeeReport1">
                        <div class="card-body">
                            <h5 class="card-title">Çalışan Bazlı Raporlar</h5>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script>
        function getDateCategories() {
            $.ajax({
                headers: { 
                    'Accept': 'application/json',
                    'Content-Type': 'application/json;charset=utf-8'
                },
                url:"/WorkCube/Odev5_AcmeRaporlama/cfc/employee.cfc?method=GetCategoryDateEmployee&date=1&categoryId=1",
                type:"GET",
                success: function(resp) {
                    
                    if(resp){
                        var list = $.parseJSON(resp);
                        var index=1;
                        list.DATA.DATA.forEach(element => {
                                
                           $('#table_categoryDate_body').append('<tr>'+
                                '<td>'+index+'</td>'+
                                '<td>'+element[0]+'</td>'+
                                '<td>'+element[1]+'</td>'+
                                '<td>'+element[2]+'</td>'+
                                '<td>'+element[3]+'</td>'+
                                '<td>'+element[4]+'</td>'+
                           '</tr>');
                           index=index+1;
                        });
                    }
                }
            });
        }
    </script>  
</body>
</html>


<cfprocessingdirective pageencoding="utf-8"/>

<cfscript>
    branchCfc = createObject('component','cfc.branch');
</cfscript>

<!DOCTYPE html>
<html lang="en">
  <head>

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="../Odev5_AcmeRaporlama/asest/css/site.css" type="text/css" media="all">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <title>RAPORLAR</title>
  </head>

   <cfparam  name="form.FormInputBranch" default= "">
   <cfset branchListResponse = branchCfc.GetBranchList()> 
   <cfset branchCfcResponse = branchCfc.GetBranchCategory(form.FormInputBranch)> 
   <cfset  data = branchCfcResponse> 
  <!--- <cfdump var = "#data#" abort> --->

  <body>
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <cfinclude template="banner.cfm">
      </nav>
    <div class="container">
            <div class="tab-content">
                <div class="card-header">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                          <a class="nav-link active" id="category-tab" data-toggle="tab" href="#categoryReport" role="tab" aria-controls="category" aria-selected="true">Kategori Rapor</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" id="date-tab" data-toggle="tab" href="#dateReport" role="tab" aria-controls="date" aria-selected="false">Tarih Rapor</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" id="product-tab" data-toggle="tab" href="#productReport" role="tab" aria-controls="product" aria-selected="false">Ürün Rapor</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="customer-tab" data-toggle="tab" href="#customerReport" role="tab" aria-controls="customer" aria-selected="false">Müşteri Rapor</a>
                          </li>
                        <li class="nav-item">
                            <a class="nav-link" id="employee-tab" data-toggle="tab" href="#employeeReport" role="tab" aria-controls="employee" aria-selected="false">Çalışan Rapor</a>
                        </li>
                      </ul>
                </div>
                <div role="tabpanel" class="tab-pane active" id="categoryReport">
                    <div class="card col col-md-12">
                        <div class="card-body">
                           <h5 class="card-title">Şubelere Göre Kategori Satış Raporu</h5>
                          <form name="BranchCategoySatis" method="POST" action="">
                              <div class="form-row align-items-center">
                                  <div class="col-sm-3 my-1 ">
                                      <input type="hidden" name="is_submit" id="is_submit" value="1">
                                      <label>Şube Seçin</label>
                                      <select class="form-control" name="FormInputBranch" id="FormInputBranch">
                                          <option value='0'>Şube Seçin</option>
                                          <cfoutput>
                                            <cfloop query=#branchListResponse.data#>
                                              <option value='#Id#'>#Name#</option>
                                            </cfloop>
                                          </cfoutput>
                                      </select>
                                  </div>
                                  <div class="col-sm-3 my-1">
                                      <input class="btn btn-primary" type="submit" value="Ara" style="margin-top: 32px;">
                                  </div>
                              </div>
                          </form>
                        </div>
                        <div class="row">
                          <cfif isdefined("form.is_submit") and (form.FormInputBranch) NEQ 0>
                            <div class="col col-md-12" name="tableRow">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th scope="col">Şube</th>
                                            <th scope="col">Kategori Adı</th>
                                            <th scope="col">Toplam Adet</th>
                                            <th scope="col">Toplam Tutar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <cfoutput>
                                            <cfloop query=#data.data#>
                                                <tr>
                                                    <td scope="row">#Sube_Adi#</td>
                                                    <td>#Kategori_Adi#</td>
                                                    <td>#Adet#</td>
                                                    <td>#Toplam_Tutar#</td>
                                                </tr>
                                            </cfloop>
                                        </cfoutput>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row" name="chartBarRow" id="branchChartBarRow">
                              <div class="col col-md-12">
                                <div class="card">
                                  <div class="card-header">
                                      Kategori Adet-Satış Grafiği
                                  </div>
                                  <div class="card-body">
                                      <cfoutput>
                                          <cfchart
                                              format="png"
                                              scalefrom="0"
                                              scaleto="3000"
                                              chartheight=300
                                              chartwidth=600>
                                              <cfchartseries
                                                  type="bar"
                                                  seriescolor="blue">
                                                  <cfloop query=#data.data#>
                                                    <cfchartdata item="#Kategori_Adi#" value="#Adet#">
                                                  </cfloop>
                                              </cfchartseries>
                                          </cfchart>
                                      </cfoutput>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="row" name="chartPieRow" id="branchChartPieRow">
                              <div class="col col-md-12">
                                <div class="card">
                                  <div class="card-header">
                                      Kategori Satış-Tutar Grafiği
                                  </div>
                                  <div class="card-body">
                                      <cfoutput>
                                          <cfchart
                                              format="png"
                                              scalefrom="100000"
                                              scaleto="10000000"
                                              chartheight=300
                                              chartwidth=600>
                                              <cfchartseries
                                                  type="pie">
                                                  <cfloop query=#data.data#>
                                                    <cfchartdata item="#Kategori_Adi#" value="#Toplam_Tutar#">
                                                  </cfloop>
                                              </cfchartseries>
                                          </cfchart>
                                      </cfoutput>
                                  </div>
                                </div>
                              </div>
                            </div>
                          <cfelse>
                            <div class="class="col col-md-12">
                                <cfoutput>
                                    <h5>Kategorilerin satış raporu için şubeyi belirleyin!</h5>
                                </cfoutput>
                            </div> 
                          </cfif>
                        </div>
                    </div>
                </div>
            
                <div role="tabpanel" class="tab-pane" id="dateReport">
                    <div class="card col col-md-12" id="dateReport1">
                        <div class="card-body">
                            <h5 class="card-title">Tarih Bazlı Raporlar</h5>
                        </div>
                    </div>
                    
                </div>

                <div role="tabpanel" class="tab-pane" id="productReport">
                    <div class="card col col-md-12" id="productReport1">
                        <div class="card-body">
                            <h5 class="card-title">Ürün Bazlı Raporlar</h5>
                        </div>
                    </div>
                </div>

                <div role="tabpanel" class="tab-pane" id="customerReport" >
                    <div class="card col col-md-12" id="customerReport1">
                        <div class="card-body">
                            <h5 class="card-title">Müşteri Bazlı Raporlar</h5>
                        </div>
                    </div>
                </div>

                <div role="tabpanel" class="tab-pane" id="employeeReport" >
                    <div class="card col col-md-12" id="employeeReport1">
                        <div class="card-body">
                            <h5 class="card-title">Çalışan Bazlı Raporlar</h5>
                        </div>
                    </div>
                </div>
            </div>
      
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
SELECT c.[Name] AS [Kategori_Adi],
CASE
	WHEN MONTH(o.ItemSoldDate) = '1'  THEN 'OCAK'
	WHEN MONTH(o.ItemSoldDate) = '2'  THEN '�UBAT'
	WHEN MONTH(o.ItemSoldDate) = '3'  THEN 'MART'
	WHEN MONTH(o.ItemSoldDate) = '4'  THEN 'N�SAN'
	WHEN MONTH(o.ItemSoldDate) = '5'  THEN 'MAYIS'
	WHEN MONTH(o.ItemSoldDate) = '6'  THEN 'HAZ�RAN'
	WHEN MONTH(o.ItemSoldDate) = '7'  THEN 'TEMMUZ'
	WHEN MONTH(o.ItemSoldDate) = '8'  THEN 'A�USTOS'
	WHEN MONTH(o.ItemSoldDate) = '9'  THEN 'EYL�L'
	WHEN MONTH(o.ItemSoldDate) = '10'  THEN 'EK�M'
	WHEN MONTH(o.ItemSoldDate) = '11'  THEN 'KASIM'
	WHEN MONTH(o.ItemSoldDate) = '12'  THEN 'ARALIK'
END AS Ay,
SUM(o.OrderTotal) AS [Toplam_Tutar],sum(p.CategoryId) as Adet
FROM [Order]  o 
INNER JOIN OrderItem oi ON o.Id = oi.Id
INNER JOIN Product p ON p.Id = oi.ProductId
INNER JOIN Category c ON c.Id = p.CategoryId
where o.OrderTotal !=0
GROUP BY MONTH(o.ItemSoldDate),c.[Name]
ORDER BY MONTH(o.ItemSoldDate),c.[Name]